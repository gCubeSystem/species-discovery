/**
 * 
 */
package org.gcube.portlets.user.speciesdiscovery.client.util;

/**
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 *
 */
public interface GridField {
	
	public String getId();
	public String getName();
	public boolean isSortable();

}
