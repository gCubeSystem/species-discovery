/**
 *
 */
package org.gcube.portlets.user.speciesdiscovery.server.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.gcube.application.framework.core.session.ASLSession;
import org.gcube.common.portal.PortalContext;
import org.gcube.common.storagehubwrapper.server.StorageHubWrapper;
import org.gcube.common.storagehubwrapper.server.tohl.Workspace;

import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * The Class GetWorkspaceUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Oct 27, 2021
 */
public class GetWorkspaceUtil {

	protected static Logger logger = Logger.getLogger(GetWorkspaceUtil.class);

	/**
	 * Gets the workspace.
	 *
	 * @param request the request
	 * @param session the session
	 * @return the workspace
	 * @throws Exception the exception
	 */
	public static Workspace getWorkspace(final HttpServletRequest request, ASLSession session) throws Exception {

		if (session == null)
			throw new Exception("ASL session is null");

		if (session.getScope() == null)
			throw new Exception("Scope into ASL session is null");

		String scope = session.getScope().toString();
		// logger.trace("Get workspace for scope "+scope);
		// ScopeProvider.instance.set(scope);
		// logger.trace("ScopeProvider instancied for scope "+scope);
		logger.trace("retuning workspace for username " + session.getUsername());
		return getStorageHubWrapper(request, null, session.getUsername()).getWorkspace();
	}

	/**
	 * Checks if is within portal.
	 *
	 * @return true if you're running into the portal, false if in development
	 */
	public static boolean isWithinPortal() {
		try {
			UserLocalServiceUtil.getService();
			return true;
		} catch (Exception ex) {
			logger.trace("Development Mode ON");
			return false;
		}
	}

	/**
	 * Gets the storage hub wrapper.
	 *
	 * @param request      the request
	 * @param scopeGroupId the scope group id. If scopeGroupId is null the scope is
	 *                     read by using the request else by using the scopeGroupId
	 * @param username     the username
	 * @return the storage hub wrapper
	 * @throws Exception the exception
	 */
	public static StorageHubWrapper getStorageHubWrapper(final HttpServletRequest request, String scopeGroupId,
			String username) throws Exception {

		if (username == null || username.isEmpty())
			throw new Exception("Session expired");

		try {
			String scope;
			PortalContext pContext = PortalContext.getConfiguration();
			if (isWithinPortal() && scopeGroupId != null) {
				scope = pContext.getCurrentScope(scopeGroupId);
				logger.debug(scope + " has retrieved by using the scopeGroupId=" + scopeGroupId);
			} else
				scope = pContext.getCurrentScope(request);

			logger.debug("Getting " + StorageHubWrapper.class.getSimpleName() + " for user: " + username
					+ " by using the scope: " + scope);
			String token = pContext.getCurrentUserToken(scope, username);
			return new StorageHubWrapper(scope, token, false, false, true);
		} catch (Exception e) {
			logger.error("Error during getting storageHub wrapper", e);
			throw new Exception("Error on gettig the StorageHub wrapper for userId: " + username);
		}
	}

}
