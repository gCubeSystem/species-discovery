package org.gcube.portlets.user.speciesdiscovery.client.externalsystem;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 *
 */
public interface ExternalSystemInterface {
	
	public String getName();
	public String getBaseUrl();	
	public String getSuffixUrl();
}
