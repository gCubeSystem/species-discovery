# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v3.11.0] - 27-10-2021

- [#21969] Removed HL dependency
- Ported to maven-portal-bom v3.6.3
- Ported to workspace-explorer v2.X.Y
- Ported to geonetwork [3.4.5,4.0.0-SNAPSHOT)
- Ported to storagehub-client-wrapper [1.0.0, 2.0.0-SNAPSHOT)
- Ported to  spd-client-library [4.1.0-SNAPSHOT, 5.0.0-SNAPSHOT)

## [v3.10.0] - 22-05-2020

- [#19221] Migrateto git/jenkins

#### Fixes
- [#19312] the WorkspaceExplorer window appears behind the Job Window (z-index issue)

## [v3.9.1] - 20-03-2017

[Bug #7568] Managed ASL session expiration during polling on SPD jobs


## [v3.9.0] - 11-01-2016

[Feature #6313] SPD portlet upgrade: porting to spd-client-library 4.0.0

[Task #7001] Create Gis Layer via job


## [v3.8.1] - 15-09-2016

Removed Gis -viewer dependency


## [v3.8.0] - 15-05-2015

Upgraded to gwt 2.6.1

Integrated with Workspace Explorer

Updated to Liferay 6.2


## [v3.7.2] - 15-05-2015

Changed dateformat at dd-MM-yyyy


## [v3.7.1] - 09-12-2014

Removed openlayers dependency from pom


## [v3.7.0] - 04-06-2014

Updated pom to support new portal configuration (gcube release 3.2)


## [v3.6.0] - 23-04-2014

Portlet updated to support service updating


## [v3.5.0] - 24-10-2013

Portlet updated to support GWT 2.5.1

Ticket 2224: was implemented


## [v3.4.0] - 06-09-2013

Enhancements on GUI of SPD portlet was realized (view last query, new expand button are available)

The functionalities was updated in order to fit the changes in the service client. New advanced options are now available: "Expand with synonyms", "Unfold the taxa group by".


## [v3.3.0] - 09-07-2013

Bug Fixed: on species classification


## [v3.2.0] - 29-05-2013

Bug Fixed: #612 (SPD: Error when saving csv file in workspace)


## [v3.1.0] - 14-04-2013

Synch with SPD service changes


## [v3.0.0] - 05-03-2013

Ticket #1260: This component has been mavenized


## [v2.3.0] - 17-01-2013

Ticket #986: Species Occurrence jobs / Re-submit job and "info"

Ticket #1002: SPD portlet persistence refactoring


## [v2.2.0] - 30-11-2012

Ticket #508: Implementation of requirements resulting from ticket #508


## [v2.0.0] - 13-09-2012

Ticket #81: Implementation of requirements resulting from user feedback

Dynamic clustering of result set items

Revised the occurrence points visualization strategy through the GIS Viewer


## [v1.0.1] - 23-07-2012

Fixed 447: Map generation fails in Species Discovery Portlet


## [v1.0.0] - 04-05-2012

First release
